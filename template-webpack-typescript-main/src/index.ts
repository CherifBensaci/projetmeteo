import { Handler } from "@netlify/functions";

const btn =  document.getElementById('btn1');
let city =  document.getElementById('ville');

let cityName = document.getElementById("city_name");
let sky = document.getElementById("ciel");
let temperatureMax = document.getElementById("temp_max");
let temperatureMin = document.getElementById("temp_min");
let windSpeed = document.getElementById("vitesse_vent");
let windDeg = document.getElementById("direction_vent");
let humiditee = document.getElementById("humidity")
let icons = document.getElementById("icons")

const handler: Handler =async (event,context) => {
  return {
    statusCode: 200,
    body: JSON.stringify({message: "Hello World!"})
  }
}

export{handler}
btn.addEventListener('click',() => {
  //@ts-ignore
  fetch("http://api.openweathermap.org/data/2.5/weather?q="+ city.value + "&units=metric&appid=b21fb10b40bba8bafbce2036a44e9643")
  .then( resp => resp.json())
  .then(function(data) {



    cityName.innerHTML = "Ville: " + data.name + "."
    sky.innerHTML = "Ciel: " + data.weather[0].main + "."

    temperatureMax.innerHTML = "Température maximale " + data.main.temp_max + "."
    temperatureMin.innerHTML = "Température minimale " + data.main.temp_min + "."
    windSpeed.innerHTML = "Vitesse du vent : " + data.wind.speed + "."
    windDeg.innerHTML = " Direction du vent : " + data.wind.deg + "."
    humiditee.innerHTML = " L'humidité de l'air " + data.main.humidity + "."
    //@ts-ignore
    icons.src = "http://openweathermap.org/img/wn/" + data.weather[0].icon + "@4x.png"





    console.log(data.name);
    console.log(data.weather[0].main);
    console.log(data.main.temp_max);
    console.log(data.main.temp_min);
    console.log(data.wind.speed);
    console.log(data.wind.deg);
    console.log(data.main.humidity);
    
    console.log(data);
  })
  .catch(function(error) {
    console.log(error);
  });
})